

// Invodo Init
Invodo.init({
  pageName: 'IVP Local Test Page',
  pageType: 'product',
  debugLogger: 'true',
  affiliate: { 'showPlayButton': 'false'}
});

// Get the element
var el = document.getElementById('example');

// Create some args
var args = {
  /// Required
  /// Pass a valid refId to get the video and data
  refId: '5B324QTK',
  /// Optional
  /// Give a name for the widget
  name: 'ixd-developer-widget',
  /// Optional
  /// Apply css normalize rules to widget
  normalize: true,
  /// Optional
  /// Manually override data fetched from the refID
  data: IVP_DATA
};

// Instantiate the IVP widget
var ivp = new Invodo.Ixd.Ivp(el, args, function(event) {
  console.log(this, event);
});
