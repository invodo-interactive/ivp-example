
var IVP_DATA = {
	"video": {
		"duration": 600,
		"dimensions": {
			"min-width": 400,
			"max-width": 600
		}
	},
	"poster": {
		"templates":[{
			"id": 1,
			"text": "<div class=\"ivp-btn ivp-btn--big-play\" role=\"button\" aria-live=\"polite\" tabindex=\"0\"><div><span>Play</span></div></div>"
		}],
		"items": [{
			"id": 1,
			"enabled": true,
			"template": 1,
			"data": {
				"image": ''
			}
		}]
	},
	"hotspot": {
		"enable": true,
		"templates":[
			{
				"id": 1,
				"text": "<span class=\"ivp-hotspot-icon\"></span><div class=\"ivp-hotspot-title\" role=\"button\" aria-live=\"polite\"><div><span>${text}</span></div></div>"
			},
			{
				"id": 2,
				"text": "<div class=\"custom-hotspot-picture\"><img src=\"${image}\"></div>"
			}
		],
		"items": [
			{
				"id": 1,
				"enabled": true,
				"template": 1,
				"actions": [
					{
						"id": 1,
						"type": "card",
						"target": 2
					},
					// {
					// 	"id": 2,
					// 	"type": "card",
					// 	"target": 3
					// },
					{
						"id": 3,
						"type": "breakpoint",
						"target": 1
					}
				],
				"data": {
					"text": "HotSpot #1 hello there!",
					"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/product-bike-2.jpg"
				},
				"time": {
					"start": 0,
					"end": 3
				},
				"position": {
					"coords": [{
						"x": 10,
						"y": 10
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 1,
						"end": 1
					},
					"from": {
						"opacity": 0
					},
					"config":[
						{
							"opacity": 1
						},
						{
							"opacity": 0,
							"scale": 0.5
						}
					]
				}
			},
			{
				"id": 2,
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 1
				}],
				"data": {
					"text": "I am HotSpot #2"
				},
				"time": {
					"start": 2.5,
					"end": 10
				},
				"position": {
					"coords": [{
						"x": 10,
						"y": 60
					}],
					"unit": "%"
				},
				"position_unit": "%",
				"animate": {
					"duration": {
						"start": 1,
						"end": 3
					},
					"config":[
						{
							"opacity": 1
						},
						{
							"opacity": 0,
							"scale": 0.5
						}
					]
				}
			},
			{
				"id": 3,
				"enabled": true,
				"template": 1,
				"actions": [{
					"id": 1,
					"type": "card",
					"target": 3
				}],
				"data": {
					"text": "I am HotSpot #3"
				},
				"time": {
					"start": 4,
					"end": 10
				},
				"position": {
					"coords": [{
						"x": 10,
						"y": 40
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 1,
						"end": 5
					},
					"config":[
						{
							"opacity": 1
						},
						{
							"opacity": 0,
							"scale": 0.5
						}
					]
				}
			},
			{
				"id": 4,
				"enabled": true,
				"template": 2,
				"actions": [
					{
						"id": 1,
						"type": "card",
						"target": 2
					},
					{
						"id": 2,
						"type": "breakpoint",
						"target": 1
					}
				],
				"data": {
					"text": "HotSpot #1 hello there!",
					"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/product-bike-2.jpg"
				},
				"time": {
					"start": 7,
					"end": 20
				},
				"position": {
					"coords": [{
						"x": 60,
						"y": 50
					}],
					"unit": "%"
				},
				"animate": {
					"duration": {
						"start": 1,
						"end": 1
					},
					"config":[
						{
							"opacity": 1
						},
						{
							"opacity": 0,
							"scale": 0.5
						}
					]
				}
			}
		]
	},
	"card": {
		"enabled": true,
		"templates":[
			{
				"id": 1,
				"text": "<div class=\"ivp-card-header\"><img src=\"${ logo }\"></div><div class=\"ivp-card-body\"><img src=\"${ product }\"><h2>${ title }</h2><p>${ text }</p></div>"
			},
			{
				"id": 2,
				"text": "<div class=\"ivp-card-header\"> \
					<div class=\"ivp-container\"> \
						<div class=\"ivp-row\"> \
							<img src=\"${logo}\"> \
						</div> \
					</div> \
				</div> \
				<div class=\"ivp-card-body\"> \
					<div class=\"ivp-container\"> \
						<div class=\"ivp-row\"> \
							<div class=\"ivp-col-7 ivp-col-pull-gutter\"><img src=\"${image}\"></div> \
							<div class=\"ivp-col-5\"> \
								<h3>${title}</h3> \
								<p>${text}</p> \
								<a class=\"ivp-cta ivp-cta-primary\">Add to Cart</a> \
								<a class=\"ivp-cta ivp-cta-primary\">Learn More</a> \
							</div> \
						</div> \
					</div> \
				</div>"
			}
		],
		"items": [{
			"id": 1,
			"template": 2,
			"data": {
				"logo": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/logo-invodo.png",
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/product-bike-2.jpg",
				"title": "HotSpot Card Title #1",
				"text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi."
			},
			"position": {
				"x": 0,
				"y": -100,
				"unit": "%"
			},
			"animate": {
				"duration": {
					"start": .5,
					"end": 1
				},
				"from": {
					"opacity": 1
				},
				"config":[
					{
						"y": '20%',
						"opacity": 1,
						"scale": 1
					},
					{
						"opacity": 0,
						"x": "100%"
					}
				]
			}
		},
		{
			"id": 2,
			"template": 2,
			"data": {
				"logo": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/logo-invodo.png",
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/product-bike-2.jpg",
				"title": "HotSpot Card Title #2222",
				"text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi."
			},
			"position": {
				"x": 0,
				"y": 10,
				"unit": "%"
			},
			"animate": {
				"duration": {
					"start": .5,
					"end": 1
				},
				"from": {
					"opacity": 0,
					"scale": .5
				},
				"config":[
					{
						"opacity": 1,
						"scale": 1
					},
					{
						"opacity": 0,
						"scale": 0.5
					}
				]
			}
		},
		{
			"id": 3,
			"template": 2,
			"data": {
				"logo": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/logo-invodo.png",
				"image": "http://invodo-ixd-production.s3.amazonaws.com/ivp/demo/product-image.jpg",
				"title": "HotSpot Card Title #3",
				"text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum felis justo, pretium sed mauris ac, ornare ultricies sapien. Cras non luctus mi."
			},
			"position": {
				"x": 0,
				"y": 20,
				"unit": "%"
			},
			"animate": {
				"duration": {
					"start": .5,
					"end": 1
				},
				"from": {
					"opacity": 0,
					"scale": .5
				},
				"config":[
					{
						"opacity": 1,
						"scale": 1
					},
					{
						"y": "-100%",
						"scale": 0.5,
						"opacity": 0
					}
				]
			}
		}]
	}
};
